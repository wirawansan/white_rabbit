<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends PHPUnit_Framework_TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    /** This function will not working properly if :
     *  - the starting amount is multiply by 7 
     *  - the starting amount < 0.50 & the multiplier is < 3
     *  - the result has a fraction value
     */
    public function multiplyProvider(){
        return array(
            array(4, 2, 2),       // success
            array(6, 3, 2),       // success
            array(14, 7, 2),      // fail
            array(0.98, 0.49, 2), // fail
            array(1.5, 0.30, 5)   // fail
        );
    }
}
