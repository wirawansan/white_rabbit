<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){  
        $result = ["1" => 0, "2" => 0, "5" => 0, "10" => 0, "20" => 0, "50" => 0, "100" => 0];
        $divider = [1, 2, 5, 10, 20, 50, 100];
        
        for ( $x = count($divider)-1; $x >= 0; $x-- ) {
            while ( $amount >= $divider[$x] && $amount % $divider[$x] >= 0 ) {
                $result[$divider[$x]]++;
                $amount -= $divider[$x];
            }
        }

        return $result;
    }
}