<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $string = file_get_contents($filePath);
        $string = preg_replace("/[^\w_]+/u", "", $string);
        $string = strtolower(preg_replace("/[_\xC0-\xDF0-9+]/", "", $string));
        
        return $string;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $letters = count_chars($parsedFile, 1);
        asort($letters);
        $midElem = round(count($letters) / 2) - 1;
        $keys = array_keys($letters);
        $midKey = $keys[$midElem];
        $occurrences = $letters[$midKey];

        return chr($midKey);
    }
}